export PYTHONPATH="${PYTHONPATH}:$(pwd)/src"
cat ./data/AComp_Passenger_data_no_error\(1\).csv \
    | python3 src/groupby/groupby_mapper.py \
    | python3 src/groupby/groupby_shuffler.py \
    | python3 src/groupby/groupby_reducer.py \
    | python3 src/max/max_mapper.py \
    | python3 src/max/max_shuffler.py \
    | python3 src/max/max_reducer.py
