from collections import defaultdict

from common.shuffler import Shuffler


class GroupByShuffler(Shuffler):
    def __init__(self) -> None:
        super().__init__()

    def shuffle(self):
        data = defaultdict(list)
        for line in self._input:
            line = line.strip()
            k, v = line.split(",")
            
            data[k].append(v)

        for k, v in sorted(data.items(), key=lambda x:x[0]):
            print(f"{k},{v}")

if __name__ == "__main__":
    shuffler = GroupByShuffler()
    shuffler()
