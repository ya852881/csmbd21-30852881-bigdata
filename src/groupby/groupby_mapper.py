import multiprocessing as mp
import sys

import numpy as np

from common.mapper import ParallelMapper

N_CORES = mp.cpu_count()    
    
class GroupByMapper(ParallelMapper):
    def __init__(self, n_cores: int):
        super().__init__(n_cores)
    
    def _row_mapper(self, line):
        line = line.strip()
        passenger_id = line.split(",")[0]
        print(f"{passenger_id},{1}")

    
if __name__ == "__main__":
    mapper = GroupByMapper(n_cores=N_CORES)
    mapper()
    

