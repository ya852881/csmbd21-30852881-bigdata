import multiprocessing as mp

from common.reducer import ParallelReducer

N_CORES = mp.cpu_count()

class GroupByReducer(ParallelReducer):
    def __init__(self, n_cores: int) -> None:
        super().__init__(n_cores)

    def _row_reducer(self, line):
        line = line.strip()
        passenger_id, num_trips = line.split(",", 1)
        try:
            num_trips = [int(i) for i in eval(num_trips)]
        except:
            pass
        print(f"{passenger_id},{sum(num_trips)}")

if __name__ == "__main__":
    reducer = GroupByReducer(n_cores=N_CORES)
    reducer()
    
