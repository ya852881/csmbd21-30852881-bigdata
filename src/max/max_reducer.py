from common.reducer import SequentialReducer


class MaxReducer(SequentialReducer):
    def __init__(self) -> None:
        super().__init__()
        
    def _seq_reducer(self, line):
        line = self.input_[0]
        values = eval(line.split(",",1)[1])
        values = [tuple(value[1:-1].split(",")) for value in values]
        values = [(pass_id, int(trips)) for pass_id, trips in values]

        max_trips = max(values, key=lambda x: x[1]) 
        print(f"Maximum number of trips ({max_trips[1]}) is done by {max_trips[0]}")
    
        
if __name__ == "__main__":
    reducer = MaxReducer()
    reducer()