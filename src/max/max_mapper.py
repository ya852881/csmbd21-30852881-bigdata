import multiprocessing as mp

from common.mapper import ParallelMapper

N_CORES = mp.cpu_count()

class MaxMapper(ParallelMapper):
    def __init__(self, n_cores: int) -> None:
        super().__init__(n_cores)
        
    def _row_mapper(self, line):
        line = line.strip()
        print(f"0,({line})")


if __name__ == "__main__":
    mapper = MaxMapper(n_cores=N_CORES)
    mapper()