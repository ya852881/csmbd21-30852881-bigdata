import multiprocessing as mp
import sys
from abc import abstractclassmethod
from typing import Any

import numpy as np


class BaseReducer:
    def __init__(self) -> None:
        self.input_ = list(sys.stdin)
    
    @abstractclassmethod
    def reducer(self):
        pass

    def __call__(self, *args: Any, **kwds: Any) -> Any:
        self.reducer()


class SequentialReducer(BaseReducer):
    def __init__(self) -> None:
        super().__init__()
        
    @abstractclassmethod
    def _seq_reducer(self, line):
        pass
    
    def reducer(self):
        for line in self.input_:
            self._seq_reducer(line)        
    
        
class ParallelReducer(BaseReducer):
    def __init__(self, n_cores: int) -> None:
       super().__init__()
       
       self.n_cores = n_cores
       self._chunks = np.array_split(self.input_, n_cores)
       
    @abstractclassmethod
    def _row_reducer(self, line):
        return
    
    def _chunk_reducer(self, chunk):
        for line in chunk:
            self._row_reducer(line)

    def reducer(self):
        pool = mp.Pool(processes=self.n_cores)
        pool.map(self._chunk_reducer, self._chunks)
        pool.close()
        pool.join()
        return