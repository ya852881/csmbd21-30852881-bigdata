import sys
from abc import abstractclassmethod
from typing import Any


class Shuffler:
    def __init__(self) -> None:
        self._input = list(sys.stdin)
        
    @abstractclassmethod
    def shuffle(self):
        return
    
    def __call__(self, *args: Any, **kwds: Any) -> Any:
        self.shuffle()