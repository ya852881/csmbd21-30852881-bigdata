import multiprocessing as mp
import sys
from abc import abstractclassmethod
from typing import Any

import numpy as np


class BaseMapper:
    def __init__(self) -> None:
        self.input_ = list(sys.stdin)
    
    @abstractclassmethod
    def mapper(self):
        pass

    def __call__(self, *args: Any, **kwds: Any) -> Any:
        self.mapper()


class SequentialMapper(BaseMapper):
    def __init__(self) -> None:
        super().__init__()
        
    @abstractclassmethod
    def _seq_mapper(self, line):
        pass
    
    def mapper(self):
        for line in self.input_:
            self._seq_mapper(line)        
    
        
class ParallelMapper(BaseMapper):
    def __init__(self, n_cores: int) -> None:
       super().__init__() 
       self.n_cores = n_cores
       self._chunks = np.array_split(self.input_, n_cores)
       
    @abstractclassmethod
    def _row_mapper(self, line):
        return
    
    def _chunk_mapper(self, chunk):
        for line in chunk:
            self._row_mapper(line)

    def mapper(self):
        pool = mp.Pool(processes=self.n_cores)
        pool.map(self._chunk_mapper, self._chunks)
        pool.close()
        pool.join()
        return